public class OfferSupport {

    //updates parent opportunity with min and max values of child offers.
    public static void updateOppMinMax(Map<Id, Offer__c> offers, Boolean isDelete) {
        Set<Id> oppIds = new Set<Id>();
        for (Offer__c offer : offers.values()) {
            oppIds.add(offer.Opportunity__c);
        }

        Map<Id, Opportunity> opps = new Map<Id, Opportunity>([SELECT Min__c, Max__c FROM Opportunity WHERE Id IN: oppIds]);

        //re-process all offers when delete occurs
        if (isDelete) {
            for (Opportunity opp : opps.values()) {
                opp.Min__c = null;
                opp.Max__c = null;
            }

            offers = new Map<Id, Offer__c>([SELECT Value__c, Opportunity__c FROM Offer__c WHERE Opportunity__c =: oppIds AND Id NOT IN: offers.keySet()]);
        }

        //analyze current data and update if needed
        Map<Id, Opportunity> oppsToUpdate = new Map<Id, Opportunity>();
        for (Offer__c offer : offers.values()) {
            if (offer.Opportunity__c == null) continue;

            Opportunity parentOpp = opps.get(offer.Opportunity__c);

            //default if min and max are null
            if (parentOpp.Min__c == null) {
                parentOpp.Min__c = offer.Value__c;
                oppsToUpdate.put(parentOpp.Id, parentOpp);
            }
            if (parentOpp.Max__c == null) {
                parentOpp.Max__c = offer.Value__c;
                oppsToUpdate.put(parentOpp.Id, parentOpp);
            }

            //testing for min and max and setting value
            if (offer.Value__c < parentOpp.Min__c) {
                parentOpp.Min__c = offer.Value__c;
                oppsToUpdate.put(parentOpp.Id, parentOpp);
            } else if (offer.Value__c > parentOpp.Max__c) {
                parentOpp.Max__c = offer.Value__c;
                oppsToUpdate.put(parentOpp.Id, parentOpp);
            }
        }
        update oppsToUpdate.values();
    }
}
