@isTest
private class OfferTriggerTest {
    @isTest static void testOfferDML() {
        Opportunity newOpp = new Opportunity(Name = 'Test Opp', StageName = 'Closed' , CloseDate = Date.Today());
        insert newOpp;
        
        Offer__c offer1 = new Offer__c(Opportunity__c = newOpp.Id, Value__c = 1);
        insert offer1;
        
        newOpp = [SELECT Min__c, Max__c FROM Opportunity WHERE Id =: newOpp.Id];
        system.debug(newOpp);
        System.assert(newOpp.Min__c == offer1.Value__c && newOpp.Max__c == offer1.Value__c);
        
        Offer__c offer2 = new Offer__c(Opportunity__c = newOpp.Id, Value__c = 2);
        insert offer2;
        
        newOpp = [SELECT Min__c, Max__c FROM Opportunity WHERE Id =: newOpp.Id];
        System.assert(newOpp.Min__c == offer1.Value__c && newOpp.Max__c == offer2.Value__c);
        
        Offer__c offer3 = new Offer__c(Opportunity__c = newOpp.Id, Value__c = 3);
        insert offer3;
        
        newOpp = [SELECT Min__c, Max__c FROM Opportunity WHERE Id =: newOpp.Id];
        System.assert(newOpp.Min__c == offer1.Value__c && newOpp.Max__c == offer3.Value__c);
        
        delete offer3;
        newOpp = [SELECT Min__c, Max__c FROM Opportunity WHERE Id =: newOpp.Id];
        System.assert(newOpp.Min__c == offer1.Value__c && newOpp.Max__c == offer2.Value__c);
        
        offer1.Value__c = -1;
        update offer1;
        newOpp = [SELECT Min__c, Max__c FROM Opportunity WHERE Id =: newOpp.Id];
        System.assert(newOpp.Min__c == offer1.Value__c && newOpp.Max__c == offer2.Value__c);
        
    }
}