trigger OfferTrigger on Offer__c (after Insert, after Update, after UnDelete, after Delete) {
    Map<Id, Offer__c> offers = (Trigger.isDelete) ? Trigger.oldMap : Trigger.newMap;
    OfferSupport.updateOppMinMax(offers, Trigger.isDelete);
}